BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SET faker.autoinit=False;
SELECT faker.name() IS NULL;

SET faker.autoinit=True;
SELECT faker.name() IS NOT NULL;

SELECT faker.faker();
SELECT faker.faker(ARRAY['de_DE','jp_JP']);
SELECT faker.faker('fr_FR');

SELECT pg_typeof(faker.name()) = 'TEXT'::REGTYPE;

SELECT count(DISTINCT name) FROM faker._functions();

ROLLBACK;
